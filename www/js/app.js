// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('kkwzfmApp', ['ionic', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })
  .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
})
.controller('AppCtrl', function($scope) {
  $scope.exitApp = function() {
    ionic.Platform.exitApp();
  }
})
.controller('HomeCtrl', function($rootScope, $scope, $sce, $state, UIUtils, $timeout, $cordovaNetwork) {

  UIUtils.showLoading();

  document.addEventListener("deviceready", function () {

    $scope.iframeURL = 'http://www.kkwzfm.com/streaming-audio/live.php?id_player=6';

    $scope.trustSrc = function (src) {
      var isOnline = $cordovaNetwork.isOnline();

      if (isOnline) {
        $timeout(function () {
          UIUtils.hideLoading();
        }, 6000);
      }

      return $sce.trustAsResourceUrl(isOnline ? src : '');
    };

    // listen for Online event
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      UIUtils.showLoading();
      $state.go($state.current, {}, {reload: true});
    });

  }, false);

})
.factory('UIUtils', function($ionicLoading) {
  var services = {
    showLoading: showLoading,
    hideLoading: hideLoading
  };

  return services;

  function showLoading() {
    // Setup the loader
    $ionicLoading.show({
      noBackdrop: true,
      template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner><p>Connecting...</p>'
    });
  }

  function hideLoading() {
    $ionicLoading.hide();
  }
})